package com.example.sharelibrary;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class SheetAdapter extends RecyclerView.Adapter<SheetAdapter.ItemHolder> {
    private List<ResolveInfo> list;
    private OnSelectPackageListener onSelectPackageListener;
    PackageManager mPm;
    Context context;

    public SheetAdapter(Context context, List<ResolveInfo> list) {
        mPm = context.getPackageManager();
        this.context = context;
        this.list = list;
    }

    @Override
    public SheetAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sheet_main, parent, false);
        return new ItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SheetAdapter.ItemHolder holder, int position) {
        holder.resolveInfo = list.get(position);
        holder.packageName = holder.resolveInfo.activityInfo.packageName;
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectPackageListener.onPackageSelect(holder.packageName, holder.resolveInfo);
            }
        });
        try {
            ApplicationInfo ai = mPm.getApplicationInfo(holder.packageName, 0);
            CharSequence appName = mPm.getApplicationLabel(ai);
            Drawable appIcon = mPm.getApplicationIcon(holder.packageName);
            holder.imageView.setImageDrawable(appIcon);
            holder.textView.setText(appName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(OnSelectPackageListener pOnSelectPackageListener) {
        onSelectPackageListener = pOnSelectPackageListener;
    }

    public static class ItemHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;
        View view;
        ResolveInfo resolveInfo;
        String packageName;

        public ItemHolder(View itemView) {
            super(itemView);
            view = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            textView = (TextView) itemView.findViewById(R.id.textView);


        }
    }
}